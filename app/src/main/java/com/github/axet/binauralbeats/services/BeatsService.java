package com.github.axet.binauralbeats.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.app.ProximityShader;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.sound.Headset;
import com.github.axet.androidlibrary.widgets.NotificationChannelCompat;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.activities.MainActivity;
import com.github.axet.binauralbeats.app.BeatsApplication;
import com.github.axet.binauralbeats.app.Sound;

public class BeatsService extends PersistentService {
    public static final String TAG = BeatsService.class.getSimpleName();

    public static final int NOTIFICATION_RECORDING_ICON = 1;

    public static String SHOW_ACTIVITY = BeatsService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = BeatsService.class.getCanonicalName() + ".PAUSE_BUTTON";
    public static String STOP_BUTTON = BeatsService.class.getCanonicalName() + ".STOP_BUTTON";

    RecordingReceiver receiver;

    String name;
    String time;
    boolean playing;
    boolean end;
    Sound sound;
    Headset headset;
    PendingIntent pause;
    PendingIntent stop;

    public class RecordingReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Headset.handleIntent(headset, intent);
        }
    }

    public static void startService(Context context, String name, String time, boolean playing, boolean end) {
        start(context, new Intent(context, BeatsService.class)
                .putExtra("name", name)
                .putExtra("time", time)
                .putExtra("playing", playing)
                .putExtra("end", end)
        );
    }

    public static void stopService(Context context) {
        stop(context, new Intent(context, BeatsService.class));
    }

    public BeatsService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

        pause = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(PAUSE_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        stop = PendingIntent.getService(this, 0,
                new Intent(this, BeatsService.class).setAction(STOP_BUTTON),
                PendingIntent.FLAG_UPDATE_CURRENT);

        sound = new Sound(this);

        receiver = new RecordingReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_MEDIA_BUTTON);
        registerReceiver(receiver, filter);
    }

    @Override
    public void onCreateOptimization() {
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, NOTIFICATION_RECORDING_ICON, BeatsApplication.PREFERENCE_OPTIMIZATION, BeatsApplication.PREFERENCE_NEXT) {
            @Override
            public void updateIcon() {
                icon.updateIcon(new Intent());
            }

            @SuppressLint("RestrictedApi")
            @Override
            public Notification build(Intent intent) {
                PendingIntent main = PendingIntent.getService(context, 0,
                        new Intent(context, BeatsService.class).setAction(SHOW_ACTIVITY),
                        PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationChannelCompat channel = new NotificationChannelCompat(context, "playing", "Playing", NotificationManagerCompat.IMPORTANCE_LOW);

                RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notifictaion);

                builder.setOnClickPendingIntent(R.id.notification_pause, pause);
                builder.setOnClickPendingIntent(R.id.notification_stop, stop);
                builder.setImageViewResource(R.id.notification_pause, !playing ? R.drawable.ic_play_arrow_black_24dp : R.drawable.ic_pause_black_24dp);

                boolean pause = !playing && !end;
                String title = name;

                if (title == null || title.isEmpty()) {
                    builder.setViewVisibility(R.id.notification_pause, View.GONE);
                    builder.setViewVisibility(R.id.notification_stop, View.GONE);
                    title = getString(R.string.app_name);
                    time = getString(R.string.optimization_killed);
                    headset(false, playing);
                } else {
                    builder.setViewVisibility(R.id.notification_pause, View.VISIBLE);
                    builder.setViewVisibility(R.id.notification_stop, View.VISIBLE);
                    if (pause)
                        title += getString(R.string.pause_notify);
                    else if (end)
                        title += getString(R.string.end_notify);
                    else
                        title += "…";
                    builder.setOngoing(!end); // ongoing until end
                    headset(true, playing);
                }

                builder.setTheme(BeatsApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                        .setChannel(channel)
                        .setMainIntent(main)
                        .setTitle(title)
                        .setText(time)
                        .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                        .setWhen(icon.notification)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setOnlyAlertOnce(true);

                return builder.build();
            }
        };
        optimization.create();
    }

    @Override
    public void onStartCommand(Intent intent) {
        String a = intent.getAction();
        if (a == null) {
            name = intent.getStringExtra("name");
            time = intent.getStringExtra("time");
            playing = intent.getBooleanExtra("playing", false);
            end = intent.getBooleanExtra("end", false);

            if (playing)
                sound.silent();
            else
                sound.unsilent();

            optimization.updateIcon();
        } else if (a.equals(PAUSE_BUTTON)) {
            Intent i = new Intent(MainActivity.PAUSE_BUTTON);
            sendBroadcast(i);
        } else if (a.equals(STOP_BUTTON)) {
            Intent i = new Intent(MainActivity.STOP_BUTTON);
            sendBroadcast(i);
        } else if (a.equals(SHOW_ACTIVITY)) {
            ProximityShader.closeSystemDialogs(this);
            MainActivity.startActivity(this);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        sound.unsilent();

        unregisterReceiver(receiver);

        headset(false, false);
    }

    void headset(boolean b, boolean playing) {
        if (b) {
            if (headset == null) {
                headset = new Headset() {
                    {
                        actions = Headset.ACTIONS_MAIN;
                    }

                    @Override
                    public void onPlay() {
                        onPause();
                    }

                    @Override
                    public void onPause() {
                        try {
                            pause.send();
                        } catch (PendingIntent.CanceledException e) {
                            Log.d(TAG, "pause canceled", e);
                        }
                    }

                    @Override
                    public void onStop() {
                        try {
                            stop.send();
                        } catch (PendingIntent.CanceledException e) {
                            Log.d(TAG, "stop canceled", e);
                        }
                    }
                };
                headset.create(this, RecordingReceiver.class);
            }
            headset.setState(playing);
        } else {
            if (headset != null) {
                headset.close();
                headset = null;
            }
        }
    }
}
